package com.rufino.calculadoranotas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rufino.calculadoranotas.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCalcular.setOnClickListener(){

            var num1 = binding.nota1
            var num2 = binding.nota2
            var num3 = binding.nota3
            var num4 = binding.nota4
            var faltas = binding.faltas
            var resultado = binding.txtResultado

            val nota1 = Integer.parseInt(num1.text.toString())
            val nota2 = Integer.parseInt(num2.text.toString())
            val nota3 = Integer.parseInt(num3.text.toString())
            val nota4 = Integer.parseInt(num4.text.toString())
            val numFaltas = Integer.parseInt(faltas.text.toString())

            val media = (nota1+nota2+nota3+nota4)/4

            if(media>=5 && numFaltas <= 20){
                resultado.setText("Aluno Aprovado \n Média final $media")
                resultado.setTextColor(getColor(R.color.green))
            }else if (numFaltas > 20){
                resultado.setText("Aluno Reprovado em Faltas")
                resultado.setTextColor(getColor(R.color.red))
            }else{
                resultado.setText("Aluno Reprovado por nota \n Média final $media")
                resultado.setTextColor(getColor(R.color.red))
            }
        }
    }
}